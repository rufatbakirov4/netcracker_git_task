1. Завести аккаунт на gitlab/github


2. Создать новый удаленный репозиторий


git clone git@gitlab.com:rufatbakirov4/netcracker_git_task.git


3. Установить git на локальную машину


4. Инициализировать локальный репозиторий


git init --initial-branch=main


5. Связать локальный репозиторий с удаленным


git remote add origin git@gitlab.com:rufatbakirov4/netcracker_git_task.git

6. Описать ранее проделанные шаги в виде TASK1.MD файла с указанием выполненных комманд


7. Закоммитить файл в локальный репозиторий


git commit -m "TASK1 with steps is commites"


8. Сделать push в удаленный репозиторий. Зайти на страницу репозитория gitlab/github, убедиться в появлении изменений


git push -u origin main

9. Дописать шаги 7-8 в файл TASK1.MD



Задание успешно выполнено!:)

