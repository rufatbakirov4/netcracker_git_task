10. Вызвать команды diff , status

git diff


git status



11. Закоммитить изменения, снова проверить статус


git commit -m "TASK1 is updated with new steps"



12. Сделать push в удаленный репозиторий


git push -u origin main


--------------------------------------------------------------


1. Шаги 10-12 описать в новом файле TASK2.MD


2. Отвести бранч develop, в который залить файл TASK2.MD. Локально и удаленно


git branch develop


git checkout develop


git add TASK2.md


git commit -m "git commit -m "TASK2 is commited to branch develop"



3. Добавить в конец файла TASK1.MD строчку "Задание провалено!:(". Залить изменения локально и удаленно


git commit -m "TASK1 is updated with new comment"


git push -u origin develop


4. Переключиться назад на основной бранч


git checkout main



5. Добавить в файл TASK1.MD строчку "Задание успешно выполнено!:)". Залить изменения локально и удаленно


git commit -m "TASK1 is updated with new comment"


git push -u origin main



6. Выполнить мерж из ветки develop в основную верку. Разрешить конфликт в пользу основной ветки


git merge develop


После разрешения конфликта в пользу ветки main:


git add TAKS1.md


git commit -m "Merged master fixed conflict."


git push -u origin main


7. Посмотреть лог проделанных изменений


git diff


git status
